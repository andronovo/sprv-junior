##########################################################
# INSTALL
##########################################################
$

cd ~
wget -c https://bitbucket.org/andronovo/sprv-junior/downloads/sprv.tar.xz
tar -Jxvf sprv.tar.xz
cd sprv
 ./setup install
cd

#
# OR
#
$
cd ~ ; wget -c https://bitbucket.org/andronovo/sprv-junior/downloads/sprv.tar.xz ; tar -Jxvf sprv.tar.xz ; cd sprv ; ./setup install ; cd

##########################################################
# UNINSTALL
##########################################################
$

cd ~
wget -c https://bitbucket.org/andronovo/sprv-junior/downloads/sprv.tar.xz
tar -Jxvf sprv.tar.xz
cd sprv
 ./setup uninstall
cd

#
# OR
#
$

cd ~ ; wget -c https://bitbucket.org/andronovo/sprv-junior/downloads/sprv.tar.xz ; tar -Jxvf sprv.tar.xz ; cd sprv ; ./setup uninstall ; cd

##########################################################
# CODE
##########################################################

SOURCE : https://bitbucket.org/andronovo/sprv-junior/src
INTERESTED : https://forum.ubuntu-tr.net/index.php?board=339.0





